import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PatientSearchComponent} from "./patient/patient-search/patient-search.component";


const routes: Routes = [
  { path: '', redirectTo: '/patient-search', pathMatch: 'full' },
  { path: 'patient-search',  component: PatientSearchComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
