import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HxUiModule} from '@hxui/angular';
import {SharedModule} from "./shared/shared.module";
import {PatientModule} from "./patient/patient.module";


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRoutingModule,
    SharedModule.forRoot(),
    HxUiModule.forRoot(),
    PatientModule
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
