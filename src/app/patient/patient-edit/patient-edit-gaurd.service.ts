import {Inject, Injectable} from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild} from '@angular/router';
import {InspectorService, InspectorSize} from '@hxui/angular';
import {PatientEditComponent} from './patient-edit.component';
import {Observable, Subscription} from 'rxjs';
import {InspectorOverlayRef} from '@hxui/angular/lib/inspector/inspector-overlay.ref';

@Injectable()
export class PatientEditGaurdService implements CanActivate, CanActivateChild {

  private patientEditInspectorRef: InspectorOverlayRef = null;
  private subscriptions: Subscription = new Subscription();

  constructor (private inspectorService: InspectorService,
               private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|boolean {
    if (!this.isInspectorOpen()) {
      this.patientEditInspectorRef = this.inspectorService.open(PatientEditComponent, {size: InspectorSize.Large}, {});
    }
    return true;
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|boolean {
    return this.canActivate(route, state);
  }


  private isInspectorOpen() {
    const inspectors = this.inspectorService['overlayCollection'];
    for (let i = 0; i < inspectors.length; i++) {
      if (inspectors[i].inspectorInstance.componentPortal.component === PatientEditComponent) {
        return true;
      }
    }
    return false;
  }


}
