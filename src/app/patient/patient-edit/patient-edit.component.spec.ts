import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PatientEditComponent } from './patient-edit.component';
import {SharedModule} from '../../shared/shared.module';
import {PatientSearchService} from '../search/patient-search.service';
import {MockBackend} from '../../../legacy-test/unit/Test.MockBackend';
import {FakeSecurityService} from '../../../test-mocks/FakeSecurityService';
import {FakeUserContextService} from '../../../test-mocks/FakeUserContextService';
import {FakeNavigationContextService} from '../../../test-mocks/FakeNavigationContextService';
import {FakeNavigationService} from '../../../test-mocks/FakeNavigationService';
import {FakeFlexAccordionService} from '../../../test-mocks/FakeFlexAccordionService';
import {FakePatientService} from '../../../test-mocks/FakePatientService';
import {FakeRootScopeService} from '../../../test-mocks/FakeRootScopeService';
import {RouterTestingModule} from '@angular/router/testing';
import {InspectorOverlayRef} from '@hxui/angular';
import {FakeInspectorOverlayRef} from '../../../test-mocks/FakeInspectorOverlayRef';

xdescribe('PatientEditComponent', () => {
  let component: PatientEditComponent;
  let fixture: ComponentFixture<PatientEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, RouterTestingModule],
      declarations: [ PatientEditComponent ],
      providers: [
        { provide: 'navigationService', useClass: FakeNavigationService },
        { provide: 'patientService', useClass: FakePatientService },
        { provide: InspectorOverlayRef, useClass: FakeInspectorOverlayRef }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
