import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {DemographicsComponent} from './demographics/demographics.component';
import {AddressComponent} from './address/address.component';
import {ContactComponent} from './contact/contact.component';
import {CarerComponent} from './carer/carer.component';
import {PayerComponent} from './payer/payer.component';
import {PreferencesComponent} from './preferences/preferences.component';
import {NotesComponent} from './notes/notes.component';
import {PatientEditGaurdService} from './patient-edit-gaurd.service';
import {BillingComponent} from './billing/billing.component';


const routes: Routes = [
  {
    path: 'patient/:id/demographics',
    component: DemographicsComponent,
    canActivate: [PatientEditGaurdService],
    outlet: 'patient-edit'
  },
  {
    path: 'patient/:id/addresses',
    component: AddressComponent,
    canActivate: [PatientEditGaurdService],
    outlet: 'patient-edit'
  },
  {
    path: 'patient/:id/contacts',
    component: ContactComponent,
    canActivate: [PatientEditGaurdService],
    outlet: 'patient-edit'
  },
  {
    path: 'patient/:id/billing',
    component: BillingComponent,
    canActivate: [PatientEditGaurdService],
    outlet: 'patient-edit'
  },
  {
    path: 'patient/:id/carer',
    component: CarerComponent,
    canActivate: [PatientEditGaurdService],
    outlet: 'patient-edit'
  },
  {
    path: 'patient/:id/payer',
    component: PayerComponent,
    canActivate: [PatientEditGaurdService],
    outlet: 'patient-edit'
  },
  {
    path: 'patient/:id/preferences',
    component: PreferencesComponent,
    canActivate: [PatientEditGaurdService],
    outlet: 'patient-edit'
  },
  {
    path: 'patient/:id/notes',
    component: NotesComponent,
    canActivate: [PatientEditGaurdService],
    outlet: 'patient-edit'
  }
];
@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})
export class PatientEditRoutingModule {}
