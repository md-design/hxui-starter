import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CarerComponent } from './carer.component';
import {SharedModule} from '../../../shared/shared.module';

describe('CarerComponent', () => {
  let component: CarerComponent;
  let fixture: ComponentFixture<CarerComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [ CarerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
