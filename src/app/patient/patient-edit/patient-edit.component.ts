import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {InspectorOverlayRef, Size} from '@hxui/angular';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {makeInitials} from '../../shared/utils/initials.func';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {PatientModel} from "../patient.model";
import {PatientService} from "../patient.service";

@Component({
  selector: 'hx-patient-edit',
  templateUrl: './patient-edit.component.html',
  styleUrls: ['./patient-edit.component.scss']
})
export class PatientEditComponent implements OnInit, OnDestroy {

  showContent$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  sizeEnum = Size;
  initials: string;
  displayName: string;
  patientSince: string;
  patientId: number;
  private subscriptions: Subscription = new Subscription();

  constructor(private inspectorOverlay: InspectorOverlayRef,
              public patientService: PatientService,
              private activeRoute: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.subscriptions.add(this.inspectorOverlay.inspectorInstance.onSlideInComplete$.subscribe(_ => this.onSlideInComplete()));
    this.subscriptions.add(this.inspectorOverlay.inspectorInstance.onSlideOutComplete$.subscribe(_ => this.onSlideOutComplete()));
    this.activeRoute.children.forEach(children =>  {
      if (children.outlet === 'patient-edit') {
        this.patientId = children.snapshot.params.id;
      }
    });
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  onSlideOutComplete() {
    this.router.navigate([{ outlets: { 'patient-edit': null } }]);
  }

  onSlideInComplete() {
    const patient: PatientModel = this.patientService.selectedPatient$.value;
    if (patient) {
      this.setPatientParts(patient);
      this.showContent$.next(true);
    }
  }

  private setPatientParts(patient: PatientModel) {
    this.displayName = (patient.PreferredName) ? patient.PreferredName : patient.Title + ' ' + patient.FirstName + ' ' + patient.Surname;
    this.initials = makeInitials(this.displayName);
    this.patientSince = patient.PatientSince;
  }

}
