import {Gender} from "./gender.enum";
import {Address} from "./address.model";
import * as moment from 'moment';

export class PatientModel {
  Id: number;
  Title: string;
  FirstName: string;
  Surname: string;
  MiddleNames: string;
  PreferredName: string;
  DateOfBirth?: Date;
  DateDeceased?: string;
  Gender: Gender;
  PatientSince?: string;
  PatientAddress: Address;
  PostalAddress: Address;
  Email: string;
  Age: string;
}
