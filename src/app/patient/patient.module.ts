import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientSearchComponent } from './patient-search/patient-search.component';
import {PatientEditComponent} from "./patient-edit/patient-edit.component";
import {DemographicsComponent} from "./patient-edit/demographics/demographics.component";
import {AddressComponent} from "./patient-edit/address/address.component";
import {ContactComponent} from "./patient-edit/contact/contact.component";
import {CarerComponent} from "./patient-edit/carer/carer.component";
import {PayerComponent} from "./patient-edit/payer/payer.component";
import {BillingComponent} from "./patient-edit/billing/billing.component";
import {PreferencesComponent} from "./patient-edit/preferences/preferences.component";
import {PatientEditRoutingModule} from "./patient-edit/patient-edit-routing.module";
import {PatientEditGaurdService} from "./patient-edit/patient-edit-gaurd.service";
import {PatientService} from "./patient.service";
import {HxUiModule} from "@hxui/angular";
import {SharedModule} from "../shared/shared.module";
import {NotesComponent} from "./patient-edit/notes/notes.component";



@NgModule({
  declarations: [
    PatientSearchComponent,
    PatientEditComponent,
    DemographicsComponent,
    AddressComponent,
    ContactComponent,
    CarerComponent,
    PayerComponent,
    BillingComponent,
    PreferencesComponent,
    NotesComponent
  ],
  entryComponents: [
    PatientEditComponent
  ],
  imports: [
    SharedModule,
    PatientEditRoutingModule
  ],
  exports: [
    PatientEditRoutingModule
  ],
  providers: [
    PatientService,
    PatientEditGaurdService
  ]
})
export class PatientModule { }
