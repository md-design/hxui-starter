import { Component, OnInit } from '@angular/core';
import {PatientService} from "../patient.service";
import {PatientModel} from "../patient.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-patient-search',
  templateUrl: './patient-search.component.html',
  styleUrls: ['./patient-search.component.scss']
})
export class PatientSearchComponent implements OnInit {

  constructor(public patientService: PatientService,
              private router: Router) { }

  ngOnInit() {
    this.patientService.searchPatients();
  }

  viewDetails(patient: PatientModel){
    this.patientService.selectPatient(patient);
  }

}
