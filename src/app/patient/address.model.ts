export class Address {
  Street1: string;
  Street2: string;
  Suburb: string;
  State: string;
  Postcode: string;
  Country: string;
}
