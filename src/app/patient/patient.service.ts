import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {PatientModel} from "./patient.model";
import {Gender} from "./gender.enum";
import * as moment from 'moment';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  patients$: BehaviorSubject<PatientModel[]> = new BehaviorSubject<PatientModel[]>([]);
  selectedPatient$: BehaviorSubject<PatientModel> = new BehaviorSubject<PatientModel>(null);

  constructor(private router: Router) { }

  selectPatient(p: PatientModel) {
    this.selectedPatient$.next(p);
    this.router.navigate([{ outlets: { 'patient-edit': ['patient', p.Id , 'demographics'] } }]);
  }

  searchPatients(){
    const patients = [];

    const patientOne = new PatientModel();
    patientOne.Id = 1;
    patientOne.Title = 'Mr';
    patientOne.FirstName = 'Sam';
    patientOne.Surname = 'Smith';
    patientOne.Gender = Gender.Male;
    patientOne.DateOfBirth = new Date('1980-07-20');
    patients.push(patientOne);

    const patientTwo = new PatientModel();
    patientTwo.Id = 2;
    patientTwo.Title = 'Mrs';
    patientTwo.FirstName = 'Michelle';
    patientTwo.Surname = 'Smith';
    patientTwo.Gender = Gender.Female;
    patientTwo.DateOfBirth = new Date('1981-05-19');
    patients.push(patientTwo);

    this.patients$.next(patients);
  }

}
