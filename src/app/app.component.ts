import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { trigger, transition, animate, style } from '@angular/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateX(-100%)'}),
        animate('500ms ease-in-out', style({transform: 'translateX(0%)'}))
      ]),
      transition(':leave', [
        animate('500ms ease-in-out', style({transform: 'translateX(-100%)'}))
      ])
    ])
  ]
})

export class AppComponent {
  nav = true;
  navs = [
    {
      cat: 'Navigation',
      items: [
        {
          name: 'Patient Search',
          link: '/patient-search'
        }
      ]
    },
    {
      cat: 'Resources',
      items: [
        {
          name: 'HxUI',
          url: 'http://www.hxui.io'
        },
        {
          name: 'HxUI Angular',
          url: 'http://angular.hxui.io'
        }
      ]
    }
  ];
}
