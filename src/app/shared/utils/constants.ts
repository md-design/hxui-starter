export class Constants {
  static readonly DATE_FMT = 'dd/MM/yyyy';
  static readonly DATE_TIME_FMT = `${Constants.DATE_FMT} h:mm a`;
  static readonly TIME_FMT = `h:mm a`;
}
