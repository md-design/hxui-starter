export function makeInitials(name: string): string {
  const sanitiseName = name.replace(/ +(?= )/g, '');
  const nameArr = sanitiseName.split(' ');
  const l1 = (nameArr.length === 3) ? nameArr[1].substr(0, 1) : nameArr[0].substr(0, 1);
  const l2 = (nameArr.length === 3) ? nameArr[2].substr(0, 1) : nameArr[1].substr(0, 1);
  return l1 + l2;
}
