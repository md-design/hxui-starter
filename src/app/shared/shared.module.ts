import {LOCALE_ID, ModuleWithProviders, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HxUiModule} from "@hxui/angular";
import {AgePipe} from "./pipes/age.pipe";
import {DateFormatPipe} from "./pipes/date-format.pipe";
import {DateTimeFormatPipe} from "./pipes/date-time-format.pipe";
import {TimeFormatPipe} from "./pipes/time-format.pipe";
import { NgSelectModule } from '@ng-select/ng-select';


@NgModule({
  declarations: [
    AgePipe,
    DateFormatPipe,
    DateTimeFormatPipe,
    TimeFormatPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    BrowserAnimationsModule,
    HxUiModule,
    NgSelectModule
  ],
  exports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    CommonModule,
    BrowserAnimationsModule,
    HxUiModule,
    AgePipe,
    DateFormatPipe,
    DateTimeFormatPipe,
    TimeFormatPipe,
    NgSelectModule
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        { provide: LOCALE_ID, useValue: 'en-AU' }
      ]
    };
  }
}
